# If length of the list is 8, then it's of the type <e><i><l><r></e>
# If length of the list is 4, then it's of the type <e><l></e>


<e lm="see you later">   <i>see<b/>you<b/>later</i><par n="hello__ij"/></e>
[['see you later', 'None', ['see', '<b/>you', '<b/>later'], ['hello__ij']]]

[[lemma, direction of transfer, <i> tag data, paradigm(s)]]

<e lm="be descended">    <i></i><par n="/be__vbser"/><p><l><b/>descended</l><r><g><b/>descended</g></r></p></e>
[['be descended', 'None', [None], [None, '<b/>descended'], [None, '<g><b/>descended</g>'], [], [], ['/be__vbser']]]

<e lm="have to">         <i>ha</i><par n="ha/ve__vbmod"/><p><l><b/>to</l><r><g><b/>to</g></r></p></e>
[['have to', 'None', ['ha'], [None, '<b/>to'], [None, '<g><b/>to</g>]', [], [], ['ha/ve__vbmod']]]

<e r="LR" lm="not be possible">    <i></i><par n="/be__vbser"/><p><l><b/>not<b/>possible</l><r><g><b/>not<b/>possible</g></r></p></e>
[['not be possible', 'LR', [None], [None, '<b/>not', '<b/>possible'], [None, '<g><b/>not<b/>possible</g>'], [], [], ['/be__vbser']]]

<e lm="take off">        <i>t</i><par n="t/ake__vblex__sep"/><par n="probj_dem__prn"/><p><l><b/>off</l><r><g><b/>off</g></r></p></e>
[['take off', 'None', ['t'], [None, '<b/>off'], [None, '<g><b/>off</g>'], [], [], ['t/ake__vblex__sep', 'probj_dem__prn']]]

[[lemma, direction of transfer, <i> tag text, <l> tag entries splie by <b/> first entry is text, [<r> text, <g> in <r>], <s> attr list in <r>, <j> attr list in <r>, list of paradigms]]
